import React,{useState,useEffect,useContext} from 'react';
import {View, StyleSheet,Text,Button , Image} from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer, StackActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TextInput } from 'react-native-gesture-handler';
import { createContext } from 'react';
import { log } from 'react-native-reanimated';
import IonIcons from 'react-native-vector-icons/Ionicons'


const Drawer = createDrawerNavigator();
const Stack =createNativeStackNavigator();
const Tab = createBottomTabNavigator();

const chat = 'Chat'
const status = 'Status'

const Auth = () => {
  return(
    <View>
      {/* <Text style={{marginTop:400,marginLeft:180,fontWeight:'bold'}}>Loading...</Text> */}
      <Image source={require('./assets/loading1.mp4')} style={{height:300,width:300}} />
    </View>
  )
}

// const Chat =() => {
//   return(
//     <Stack.Navigator>
//       <Stack.Screen name="Chatlist" component={ChatList} /> 
//       <Stack.Screen name="ChatScreen" component={ChatScreen} /> 
//       <Stack.Screen name="UserDetails" component={UserDetails} /> 
//     </Stack.Navigator>
//   )

// }

const Loggedin = () => {
  return(
   <Drawer.Navigator>
      <Drawer.Screen name='Home' component={Home}  />
      <Drawer.Screen name='Notifications' component={Notifi} />
   </Drawer.Navigator>
  )

}

const Home = () => {
  return (
    <Tab.Navigator screenOptions={({route})=>({
      tabBarIcon:({focused,color,size})=>{
          let iconName;
          let rn = route.name;
          if(rn===chat){
            iconName = focused ? 'chatbubbles' : 'chatbubbles-outline'
          }
          else if(rn===status){
            iconName = focused ? 'eye' : 'eye-outline'
          }
          return <IonIcons name={iconName} size={size} color={color} />
      },
    })}>
      <Tab.Screen name='Chat' component={Chat} options={{headerShown:false}} />
      <Tab.Screen name='Status' component={Status} />
    </Tab.Navigator>
  )
}


const Chat = () => {
  return (
      <Stack.Navigator >
        <Stack.Screen name='Screen1' component={Screen1} options={{headerShown:false}} />
        <Stack.Screen name='Screen2' component={Screen2} options={{headerShown:false}} />
      </Stack.Navigator>
  )
}


const Screen1 = ({navigation}) => {
  return (
    <View style={{marginTop:300}}>
    <Text style={{marginLeft:200}}>Screen 1</Text>
    <Button title='Screen 2' onPress={()=>navigation.navigate('Screen2')} ></Button>
    </View>
  )
}

const Screen2 = ({navigation}) => {
  return (
    <View style={{marginTop:300}}>
    <Text>Screen 2</Text>
    <Button title='Screen 1' onPress={()=>navigation.navigate('Screen1')} ></Button>
    </View>
  )
}

 

const Status = () => {
  return (
    <Text>Status</Text>
  )
}



const Notifi = () => {
  return (
    <View>
      <Text>Notifications</Text>
    </View>
  )
}
//Not Logged in Starts Here

const Notloggedin = () => {
  return(
   <Stack.Navigator initialRouteName='Login'>
      <Stack.Screen name="Login" component={Login}  />
      <Stack.Screen name="Signin" component={Signin} />
   </Stack.Navigator>
  )

}

const Login = ({navigation}) => {
  const [Name,setName] = useState();
  return(
   <View style={{marginLeft:100}} >
    <TextInput placeholder='Type Name' value={Name} onChangeText={(text)=>setName(text)} style={{marginTop:250,height:50,width:200,borderWidth:2,borderRadius:10,fontSize:15}}> </TextInput>
   <View style={{marginRight:140}}>
   <Button title="Sign In" onPress={()=>navigation.navigate('Signin',Name)}>
    </Button>
    </View>
   </View>

  )
}

const Signin= ({route,navigation}) => {
  const Name = route.params;
  console.log(Name);
  return(
   <View >
    <Text style={{marginTop:200,marginLeft:100,fontWeight:'bold'}}>
       Hello there, {Name}
    </Text>
      
   </View>
  )
}













const App = () => {
  const [Loading,setLoading] = useState(true)
  const [isLoggedIn,setisLoggedIn] = useState(false)
  
 
  
    useEffect(()=>{
      setTimeout(()=>{
        setLoading(false);
      },2500)
    },[])
      
  return (
   <NavigationContainer>
    {
      Loading ? <Auth /> : isLoggedIn ? <Loggedin /> : <Notloggedin/> 
      }
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({})

export default App;
